import React from 'react';
import { AppNavigator } from './src/AppNavigator';

export const App = () => {
  return (
    <AppNavigator />
  )
}