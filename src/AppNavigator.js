import React from 'react';
import { StackNavigator } from 'react-navigation';
import { Main } from './screens/Main';
import { SubMain } from './screens/SubMain'; 

export const AppNavigator = StackNavigator({
    Main: {
        screen: Main,
        navigationOptions: {
            header: null
        }
    },
    SubMain: {
        screen: SubMain,
        navigationOptions: { header: null }
    }
})
