import React, { Component } from 'react';
import { Form, Text, Button } from 'native-base';
import { Analytics, Hits } from 'react-native-google-analytics';
import DeviceInfo from 'react-native-device-info';

let clientId = DeviceInfo.getUniqueID();
const tracker = new Analytics('UA-112900773-1', clientId, 1, DeviceInfo.getUserAgent())

export class Main extends Component {
    componentWillMount() {
        let screenView = new Hits.ScreenView(
            'TRIGGERED',
            'screen 1',
            '1.0.5',
            DeviceInfo.getReadableVersion(),
            DeviceInfo.getBundleId()
        )
        tracker.send(screenView)
    }
    
    onPressNav() {
        const { navigate } = this.props.navigation;
        navigate('SubMain')
    }

    onEvent() {
        let _Event = new Hits.Event(
            'Event',
            'TRIGERRED',
            'React-Native',
            100
        )
        tracker.send(_Event)
    }

    render() {
        return (
            <Form style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <Text> You have been tracked </Text>

                <Form style={{ flexDirection: 'row' }}>
                    <Button primary onPress={this.onEvent.bind(this)}>
                        <Text> Click Me </Text>
                    </Button>

                    <Button success onPress={this.onPressNav.bind(this)}>
                        <Text> Go to Screen 2 </Text>
                    </Button>
                </Form>
            </Form>
        )
    }
}