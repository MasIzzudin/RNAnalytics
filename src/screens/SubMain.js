import React, { Component } from 'react';
import { 
    Form, Text, Button, Header, Left, Body, Right,
    Icon, Title

} from 'native-base';
import DeviceInfo from 'react-native-device-info';
import { Analytics, Hits } from 'react-native-google-analytics';

let clientId = DeviceInfo.getUniqueID();
const tracker = new Analytics('UA-112900773-1', clientId, 1, DeviceInfo.getUserAgent())

export class SubMain extends Component {
    componentWillMount() {
        let screenView = new Hits.ScreenView(
            'TRIGGERED',
            'screen 2',
            '1.0.5',
            DeviceInfo.getReadableVersion(),
            DeviceInfo.getBundleId()
        )
        tracker.send(screenView)
    }
    

    onPress() {
        const { navigate } = this.props.navigation;
        navigate('Main');
    }
    render() {
        return (
            <Form>
               <Header>
                    <Left>
                        <Button transparent onPress={this.onPress.bind(this)}>
                            <Icon name='arrow-back' />
                        </Button>
                    </Left>
                    <Body>
                        <Title> Screen 2 </Title>
                    </Body>

                    <Right />
                </Header>
            </Form>
        )
    }
}